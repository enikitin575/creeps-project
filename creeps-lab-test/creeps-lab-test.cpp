#include "pch.h"
#include "CppUnitTest.h"
#include "../creeps-main/protocol.h"
#include "../creeps-main/process.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace creepslabtest
{
	protocol* add_protocol(int startHour, int startMinute, int startSecond, int finishHour, int finishMinute, int finishSecond, int byteIn, int byteOut) {
		protocol* protocols = new protocol;
		protocols->start.hour = startHour;
		protocols->start.minute = startMinute;
		protocols->start.second = startSecond;
		protocols->finish.hour = finishHour;
		protocols->finish.minute = finishMinute;
		protocols->finish.second = finishSecond;
		protocols->byteIN = byteIn;
		protocols->byteOUT = byteOut;
		return protocols;
	}
	void del_protocol(protocol* array[], int size) {
		for (int i = 0; i < size; i++) {
			delete array[i];
		}
	}
	TEST_CLASS(creepslabtest)
	{
	public:
		
		TEST_METHOD(creeps_test1)
		{
			protocol* protocols[3];
			protocols[0] = add_protocol(0, 0, 0, 1, 0, 0, 512, 512);
			protocols[1] = add_protocol(12, 0, 0, 1, 0, 0, 3123, 512);
			protocols[2] = add_protocol(13, 30, 35, 12, 0, 0, 65536, 32768);
			Assert::AreEqual(131365, process(protocols, 3));
			del_protocol(protocols, 3);
		}
		TEST_METHOD(creeps_test2)
		{
			protocol* protocols[3];
			protocols[0] = add_protocol(0, 0, 0, 1, 0, 0, 512, 512);
			protocols[1] = add_protocol(12, 0, 0, 1, 0, 0, 3123, 512);
			protocols[2] = add_protocol(13, 30, 35, 12, 0, 0, 65536, 32768);
			Assert::AreEqual(131365, process(protocols, 3));
			del_protocol(protocols, 3);
		}
		TEST_METHOD(creeps_test3)
		{
			protocol* protocols[3];
			protocols[0] = add_protocol(0, 0, 0, 1, 0, 0, 512, 512);
			protocols[1] = add_protocol(12, 0, 0, 1, 0, 0, 3123, 512);
			protocols[2] = add_protocol(13, 30, 35, 12, 0, 0, 65536, 32768);
			Assert::AreEqual(131365, process(protocols, 3));
			del_protocol(protocols, 3);
		}
		TEST_METHOD(creeps_test4)
		{
			protocol* protocols[3];
			protocols[0] = add_protocol(0, 0, 0, 1, 0, 0, 512, 512);
			protocols[1] = add_protocol(12, 0, 0, 1, 0, 0, 3123, 512);
			protocols[2] = add_protocol(13, 30, 35, 12, 0, 0, 65536, 32768);
			Assert::AreEqual(131365, process(protocols, 3));
			del_protocol(protocols, 3);
		}
		TEST_METHOD(creeps_test5)
		{
			protocol* protocols[3];
			protocols[0] = add_protocol(0, 0, 0, 1, 0, 0, 512, 512);
			protocols[1] = add_protocol(12, 0, 0, 1, 0, 0, 3123, 512);
			protocols[2] = add_protocol(13, 30, 35, 12, 0, 0, 65536, 32768);
			Assert::AreEqual(131365, process(protocols, 3));
			del_protocol(protocols, 3);
		}
	};
}
