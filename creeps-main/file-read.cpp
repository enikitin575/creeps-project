#include "file-read.h"
#include "const.h"

#include <fstream>
#include <cstring>

Time convert(char* str)
{
    Time time{};
    char* context = NULL;
    char* str_number = strtok_s(str, ":", &context);
    time.hour = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    time.minute = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    time.second = atoi(str_number);
    return time;
}

void read(const char* file_name, protocol* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            protocol* item = new protocol;
            file >> tmp_buffer;
            item->start = convert(tmp_buffer);
            file >> tmp_buffer;
            item->finish = convert(tmp_buffer);
            file >> item->byteIN;
            file >> item->byteOUT;
            file.read(tmp_buffer, 1); // чтения лишнего символа пробела
            file.getline(item->name, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "Ошибка открытия файла";
    }
}